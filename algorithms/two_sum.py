from typing import List


class Solution:
    def two_sum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        pass

    @staticmethod
    def two_sum_bruce_force(nums, target):
        """
        Bruce force
        :param nums:
        :param target:
        :return:
        """
        for i in range(len(nums)):
            for j in range(i + 1, len(nums)):
                value = abs(target - nums[i])
                if nums[j] == value:
                    return i, j
                else:
                    continue

    @staticmethod
    def two_sum_hashmap(nums, target):
        """
        Hashmap
        :param nums:
        :param target:
        :return:
        """
        hashmap = {}
        for i in range(len(nums)):
            hashmap[nums[i]] = i

        for i in range(len(nums)):
            complement = target - nums[i]
            if complement not in hashmap or hashmap[complement] == i:
                continue

            return i, hashmap[complement]

    @staticmethod
    def two_sum_better_hashmap(nums, target):
        """
        Better hashmap solution
        :param nums:
        :param target:
        :return:
        """
        # Initial hashmap with first key, value pair. for later loop
        hashmap = {nums[0]: 0}
        for i in range(1, len(nums)):
            if target - nums[i] in hashmap:
                return i, hashmap[target - nums[i]]

            else:
                hashmap[nums[i]] = i
